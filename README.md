# Behaviour-Driven Development

Behavior Driven Development (BDD ou ainda uma tradução Desenvolvimento Guiado por Comportamento) é uma técnica de desenvolvimento Ágil que encoraja colaboração entre desenvolvedores, setores de qualidade e pessoas não-técnicas ou de negócios num projeto de software. Foi originalmente concebido em 2003, por Dan North [1] como uma resposta à Test Driven Development (Desenvolvimento Guiado por Testes), e tem se expandido bastante nos últimos anos.[2]

Os focos do BDD são a linguagem e as interações usadas no processo de desenvolvimento de software. Desenvolvedores usam sua língua nativa em combinação com a linguagem ubíqua (ubiquitous language), que lhes permite concentrar nas razões pelas quais o código deve ser criado, e não em detalhes técnicos, além de minimizar traduções entre a linguagem técnica na qual o código é escrito e outras linguagens de domínio, usuários, clientes, gerência do projeto, etc.

Dan North criou o primeiro framework de BDD, JBehave[1] , em Java, seguido de um framework em Ruby a nível de história chamado RBehave[1] , o qual foi depois incorporado ao projeto RSpec. Ele também trabalhou com David Chelimsky, Aslak Hellesøy e outros para desenvolver o framework RSpec e também escrever "The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends". O primeiro framework baseado em histórias no RSpec foi posteriormente substituído pelo Cucumber[3] , desenvolvido principalmente por Alask Hellesøy.

Fonte: [Wikipedia](https://pt.wikipedia.org/wiki/Behavior_Driven_Development)